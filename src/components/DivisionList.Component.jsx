import Dom7 from 'dom7';
import { ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { getEvent, getPageDataEvent } from './../global/global.Saga';
import { setGlobalParam } from './../global/global.Reducer';

class DivisionList extends PureComponent {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(getEvent({ api: 'bizInsight/getDivisions', storage: 'divisionlist_state' }));
    }
    useSelectDivsion(division_id) {
        const { dispatch, current_page } = this.props;
        dispatch({ type: setGlobalParam, storage: 'selected_division_id', response: division_id });
        dispatch(getPageDataEvent({ page: current_page }));

        this.showLoading();
        this.useChangeDom();

    }
    useChangeDom() {
        const { changeDOM } = this.props;
        if (changeDOM) {
            var $$ = Dom7;
            $$(changeDOM.html).html('');
            $$(changeDOM.prop).prop('selected', true);
        }
    }


    render() {
        const {
            divisionlist_state,
        } = this.props;

        return (
            <ul>
                <ListItem
                    title="Divsions"
                    smartSelect
                    id="divisions"
                    smartSelectParams={{ openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search Division', closeOnSelect: true }}
                >
                    <select name="divisions" onChange={(e) => this.useSelectDivsion(e.target.options.selectedIndex)}>
                        <option value="" >All</option>
                        {
                            divisionlist_state ?
                                divisionlist_state.map((value, index) => {
                                    return (<option key={index} value={value.id}>{value.code}</option>)
                                }) : ''}
                    </select>
                </ListItem>
            </ul>
        );
    }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(DivisionList);
