import 'flatpickr/dist/themes/material_green.css';
import { Block, Button, Col, Icon, List, ListItem, Popover, Row } from 'framework7-react';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from '../global/global.Reducer';
const format_global = ['D MMM', 'MMM YY', 'YYYY'];
const date_format = 'YYYY-MM-DD';

class SelectDateComponent extends PureComponent {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
    }
    
    selectDate(dateType) {
        const { dispatch, date_type } = this.props;
        let date_from = '';
        let date_to = '';
        let date = '';

        dispatch({ type: setGlobalParam, storage: 'date_type', response: dateType })
        dispatch({ type: setGlobalParam, storage: 'date_index', response: 0 });

        date_from = moment().startOf(dateType).format(date_format);
        date_to = moment().endOf(dateType).format(date_format);
        dispatch({ type: setGlobalParam, storage: 'date_from', response: date_from });
        dispatch({ type: setGlobalParam, storage: 'date_to', response: date_to });

        switch (dateType) {
            case 'day':
                date = moment().format(format_global[0]);
                dispatch({ type: setGlobalParam, storage: 'format', response: format_global[0] })
                dispatch({ type: setGlobalParam, storage: 'date', response: date });
                break;
            case 'month':
                date = moment().format(format_global[1]);

                dispatch({ type: setGlobalParam, storage: 'format', response: format_global[1] });
                dispatch({ type: setGlobalParam, storage: 'date', response: date });
                break;
            case 'year':
                date = moment().format(format_global[2]);
                dispatch({ type: setGlobalParam, storage: 'format', response: format_global[2] });
                dispatch({ type: setGlobalParam, storage: 'date', response: date });
                break;
            default:
                break;
        }
        if (dateType != date_type) {
            this.showLoading();
        }
    }

    plusOrMinusDate(param) {
        const { format, date_type, date_index, dispatch } = this.props;
        let index = date_index;
        let date = '';
        let date_from_and_to = '';
        if (param == 'add') {
            index++;
        } else {
            index--;
        }
        dispatch({ type: setGlobalParam, storage: 'show_loading', response: true });

        date = moment().add(index, date_type).format(format);
        date_from_and_to = moment().add(index, date_type).format(date_format);
        let from = moment(date_from_and_to).startOf(date_type).format(date_format);
        let to = moment(date_from_and_to).endOf(date_type).format(date_format);
        dispatch({ type: setGlobalParam, storage: 'date_from', response: from });
        dispatch({ type: setGlobalParam, storage: 'date_to', response: to });

        dispatch({ type: setGlobalParam, storage: 'date', response: date });
        dispatch({ type: setGlobalParam, storage: 'date_index', response: index });

    }

    render() {
        return (
            <Block id="selectDate" style={{
                height: 0,
                minHeight: 0,
                marginTop: 10
            }}>
                <Row noGap>
                    <Col width='15'>
                        <Button onClick={() => this.plusOrMinusDate('minus')}><Icon ios="f7:chevron_left" md="material:keyboard_arrow_left" /></Button>
                    </Col>
                    <Col width='65' style={{ textAlign: 'center' }}>
                        <Button popoverOpen=".popover-menu" >{this.props.date}</Button>
                        <Popover className="popover-menu">
                            <List>
                                <ListItem onClick={() => this.selectDate('day')} link="#" popoverClose title="Today" />
                                <ListItem onClick={() => this.selectDate('month')} link="#" popoverClose title="This Month" />
                                <ListItem onClick={() => this.selectDate('year')} link="#" popoverClose title="This Year" />
                            </List>
                        </Popover>
                    </Col>
                    <Col width='15'>
                        <Button onClick={() => this.plusOrMinusDate('add')}><Icon ios="f7:chevron_right" md="material:keyboard_arrow_right" /></Button>
                    </Col>
                </Row>
            </Block>
        );
    }

}

export default connect(state=>({...state.global}), dispatch => ({ dispatch }))(SelectDateComponent);
