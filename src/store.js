// Import redux modules, logger and devtool extension
import { createStore } from 'redux';
import { createLogger } from "redux-logger";
import { composeWithDevTools } from 'redux-devtools-extension';

// Import middleware creator
import { applyMiddleware } from 'redux';
import createSagaMiddleware from "redux-saga";
import rootReducer from "./config/reducer";
import rootSaga from "./config/sagas";
import { routerMiddleware } from "react-router-redux";
import { createHashHistory } from "history";
export const history = createHashHistory({
    hashType: 'noslash'
});
const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];
if (process.env.NODE_ENV !== 'production') {
    middlewares.push(createLogger());
}

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(
            ...middlewares,
            routerMiddleware(history)
        )
    )
);

sagaMiddleware.run(rootSaga);

export default store;