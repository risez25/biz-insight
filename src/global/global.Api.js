import axios from 'axios';
import { toast } from 'react-toastify';
const URL = localStorage.getItem('URL');
// const URL = 'localhost/ssc';
export const postFormApi = (api, form) => {
    return axios({
        method: 'post',
        url: `http://${URL}/index.php?r=${api}`,
        data: form,
        config: { headers: { 'Content-Type': 'multipart/form-data' } },
        validateStatus: (status) => {
            return true;
        },
    }).then((response) => {
        if (response.data) {
            if (response.data.STATUS == 'FAIL') {
                toast.error(response.data.MESSAGE);
                return;
            }
        }
        return response.data;
    }).catch((error) => {
        toast.error('Network Error');
    });
};

export const getFormApi = (api, storage, auth) => {
    return axios({
        method: 'get',
        url: `http://${URL}/index.php?r=${api}`,
        params: {
            username: auth.username,
            password: auth.password,
            rememberMe: localStorage.getItem('remember_me'),
        },
        validateStatus: (status) => {
            return true;
        },
    }).then((response) => {
        // if (!response.data.success) {
        // handleResponse(response.data);
        // }
        return response.data;
    }).catch((error) => {
        toast.error('Network Error');
        // swal('Error', `${error}`, 'error')
    });
};
