import "babel-polyfill";
import moment from 'moment';
import { smart_select_component } from "./state/smartselect.component.state";
import { divisionlist_component_state } from "./state/divisionlist.component.state";
import { sales_state } from "./state/sales_params.state";
import { collection_state } from './state/collection_params.state';
import { return_state } from "./state/return_params.state";
import { comparison_state } from "./state/comparison_params.ComparisonPage.state";
const initialState = {
    ...smart_select_component,
    ...divisionlist_component_state,
    ...sales_state,
    ...collection_state,
    ...return_state,
    ...comparison_state,
    rows: [],
    isAuthenticated: false,
    salesman_team_list: [],
    date: 'Today',
    format: 'D MMM',
    date_type: 'day',
    date_index: 0,
    date_from: moment().format('YYYY-MM-DD'),
    date_to: moment().format('YYYY-MM-DD'),
    show_loading: false,
    user_id: '',
    salesteam_id: 0,
    // user_id: 1,
    // role:'',
    current_page: '',
    role: '',
    username: '',
    is_salesteam_change: null,
    sales_id: 0,
    total_visit_outlet: { total_visit: 0, total_outlet: 0 },
}
export default function reducer(
    state = {
        ...initialState
    }
    , action) {
    switch (action.type) {
        case setGlobalParam: {
            return {
                ...state,
                [action.storage]: action.response
            }
        }
        case setIsAuthenticated: {
            return {
                ...state,
                isAuthenticated: action.response
            }
        }
        case setSalesPageToDefault: {
            return {
                ...state,
                ...smart_select_component,
                ...sales_state,
                ...collection_state,
                ...return_state,
                sales_id: 0,
                is_salesteam_change: null,
                total_visit_outlet: { total_visit: 0, total_outlet: 0 },
            }
        }
        case logout: {
            return {
                ...initialState
            }
        }
        default: {
            return state;
        }
    }
}


export const setIsAuthenticated = "setIsAuthenticated->GlobalReducer";
export const setGlobalParam = 'setGlobalList->GlobalReducer';
export const setSalesPageToDefault = 'setSalesPageToDefault->GlobalReducer';
export const logout = "logout->GlobalReducer";




