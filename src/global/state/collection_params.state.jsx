export const collection_state = {
    is_collection_accoridon_open: false,
    collection_toggle: false,
    collection_target_obj: {},
    collection_accordion_data: [],
    collection_chart_data: {},
    total_collection_target: 0,
    total_collection_percentage: 0,
    total_collection_amt: 0,
}