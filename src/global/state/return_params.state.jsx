export const return_state = {
    is_return_accordion_open: false,
    return_toggle: false,
    return_target_obj: {},
    return_accordion_data: [],
    return_chart_data: {},
    total_return_target: 0,
    total_return_percentage: 0,
    total_return_amt: 0,
}