export const sales_state = {
    is_sales_accordion_open: false,
    sales_toggle: false,
    sales_target_obj: {},
    sales_accordion_data: {
        inv: [],
        so: []
    },
    sales_chart_data: {},
    total_sales_target: 0,
    total_sales_percentage: 0,
    total_sales_amt: 0,
    total_inv_amt: 0,
}