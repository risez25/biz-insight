import "regenerator-runtime/runtime";
import { call, put, takeEvery, select, takeLatest } from 'redux-saga/effects';
import { setGlobalParam, setIsAuthenticated } from "./global.Reducer";
import swal from 'sweetalert2';
import { postFormApi, getFormApi } from "./global.Api";
import CryptoJS from "crypto-js";
import moment from 'moment';
import { toast } from 'react-toastify';


//Action
export const getEvent = ({ api, storage }) => ({
    type: getEventSaga,
    api: api,
    storage: storage
});

export const postEvent = ({ api }) => ({
    type: postEventSaga,
    api: api,
});

export const getPageDataEvent = ({ page }) => ({
    type: getPageDataSaga,
    page: page
});

//saga reducer
const getEventSaga = 'getEvent->GlobalSaga';
const postEventSaga = 'postEvent->GlobalSaga';
const getPageDataSaga = 'getPageData->GlobalSaga';

export const global = [
    takeEvery(getEventSaga, getEventFunc),
    takeEvery(postEventSaga, postEventFunc),
    takeLatest(getPageDataSaga, getPageDataFunc)
];

//saga function
function* getEventFunc(params) {
    try {
        const form = yield select(formSelector);
        let auth = {
            username: form.username,
            password: form.password,
        }
        const response = yield call(getFormApi, params.api, params.storage, auth);
        console.log(response);

        yield put({ type: setGlobalParam, storage: params.storage, response: response })

    } catch (error) {
        console.log(error);

    }
}

function* postEventFunc(params) {
    try {
        const form = yield select(formSelector);
        let bodyFormData = new FormData();
        localStorage.setItem('username', form.username)
        let ciphertext = CryptoJS.AES.encrypt(form.password, 'bizinsight');
        localStorage.setItem('password', ciphertext)
        bodyFormData.set('UserLogin[username]', form.username);
        bodyFormData.set('UserLogin[password]', form.password);

        yield put({ type: setGlobalParam, storage: 'show_loading', response: true });
        const response = yield call(postFormApi, params.api, bodyFormData)
        if (response) {
            if (response.STATUS == 'OK') {
                if (response.role == 'salesmanager') {
                    yield put({ type: setGlobalParam, response: true, storage: 'is_salesteam_change' })
                    yield put({ type: setGlobalParam, response: response.salesteam_id, storage: 'salesteam_id' })
                    yield put({ type: setGlobalParam, response: response.salesteam_id, storage: 'sales_id' })
                }
                if (response.role == 'salesman') {
                    yield put({ type: setGlobalParam, response: response.id, storage: 'sales_id' })
                }
                yield put({ type: setGlobalParam, response: response.role, storage: 'role' })
                yield put({ type: setGlobalParam, response: response.username, storage: 'username' })

                yield put({ type: setIsAuthenticated, response: true })
                localStorage.setItem('isAuthenticate', true);
            }
        } else {
            yield put({ type: setGlobalParam, storage: 'show_loading', response: false });
        }
    } catch (error) {
        swal({ type: 'error', text: error.message, backdrop: false, toast: true });
    }
}

function* getPageDataFunc(params) {
    yield put({ type: setGlobalParam, storage: 'current_page', response: params.page });
    switch (params.page) {
        case '/':
            yield call(getSalesDashboardPageFunc);
            break;
        case '/comparison/Salesman(Sales)':
            yield call(getComparisonSalesmanSalesFunc);
            break;
        case '/comparison/Salesman(Collection)':
            yield call(getComparisonSalesmanCollectionFunc);
            break;
        case '/comparison/Salesteam':
            yield call(getComparisonSalesteamFunc);
            break;
        case '/comparison/Product':
            yield call(getComparisonProductFunc);
            break;
        default:
            break;
    }
}

function* getSalesDashboardPageFunc() {
    const state = yield select(state => (state.global));
    const { is_salesteam_change, sales_id, date_from, date_to, selected_division_id, date_type, role } = state;

    try {
        yield put(getEvent({ api: `bizInsight/GetSalesTarget&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'sales_target_obj' }));
        yield put(getEvent({ api: `bizInsight/GetCollectionTarget&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'collection_target_obj' }));
        yield put(getEvent({ api: `bizInsight/salesmanTeamList&user_id=${sales_id}&role=${role}&division_id=${selected_division_id}`, storage: 'salesman_team_list' }));
        yield put(getEvent({ api: `bizInsight/getSalesmanVisitAndOutlet&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}`, storage: 'total_visit_outlet' }));
        switch (date_type) {
            case 'day':
                let date_start_week = moment(date_from).startOf('isoWeek').format('YYYY-MM-DD');
                let date_end_week = moment(date_to).endOf('isoWeek').format('YYYY-MM-DD');

                yield put(getEvent({ api: `bizInsight/getSalesByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getCollectionByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getReturnByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                break;
            case 'month':
                yield put(getEvent({ api: `bizInsight/getSalesByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getCollectionByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getReturnByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                break;
            case 'year':
                yield put(getEvent({ api: `bizInsight/getSalesByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getCollectionByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                yield put(getEvent({ api: `bizInsight/getReturnByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                break;
            default:
                break;
        }

    } catch (error) {
        toast.error(error);
    }
}

function* getComparisonSalesmanSalesFunc(params) {
    const state = yield select(state => (state.global));
    const { date_from, date_to, chart_limit, selected_division_id } = state;
    try {
        yield put(getEvent({ api: `bizInsight/getTopSalesman&date_from=${date_from}&date_to=${date_to}&limit=${chart_limit}&division_id=${selected_division_id}`, storage: 'comparison_pie_data' }));
    } catch (error) {
        console.log(error);
    }
}

function* getComparisonSalesmanCollectionFunc(params) {
    const state = yield select(state => (state.global));
    const { date_from, date_to, chart_limit, selected_division_id } = state;
    try {
        yield put(getEvent({ api: `bizInsight/getTopSalesmanCollection&date_from=${date_from}&date_to=${date_to}&limit=${chart_limit}&division_id=${selected_division_id}`, storage: 'comparison_pie_data' }));
    } catch (error) {
        console.log(error);
    }
}

function* getComparisonSalesteamFunc(params) {
    const state = yield select(state => (state.global));
    const { date_from, date_to, chart_limit, selected_division_id } = state;
    try {
        yield put(getEvent({ api: `bizInsight/getTopSalesTeam&date_from=${date_from}&date_to=${date_to}&limit=${chart_limit}&division_id=${selected_division_id}`, storage: 'comparison_pie_data' }));
    } catch (error) {
        console.log(error);
    }
}

function* getComparisonProductFunc(params) {
    const state = yield select(state => (state.global));
    const { date_from, date_to, chart_limit, selected_division_id } = state;
    try {
        yield put(getEvent({ api: `bizInsight/getTopSalesProduct&date_from=${date_from}&date_to=${date_to}&limit=${chart_limit}&division_id=${selected_division_id}`, storage: 'comparison_pie_data' }));
    } catch (error) {
        console.log(error);
    }
}

const formSelector = (state) => {
    if (state.form.form !== undefined) {
        return (state.form.form.values)
    }
    return '';
};
