import React, { Component, lazy, Suspense } from 'react';
import 'flatpickr/dist/themes/material_green.css';
import { Link, Navbar, NavLeft, NavTitle, Page, List, ListItem, Icon, Toggle, Row, Col, Label } from 'framework7-react';
import { connect } from "react-redux";
import SelectDateComponent from '../../components/selectDate.Component';
import { setGlobalParam } from './../../global/global.Reducer';
import { getPageDataEvent } from './../../global/global.Saga';

const DivisionListComponent = React.lazy(() => import('../../components/DivisionList.Component'));
const Chart = React.lazy(() => import('./component/Chart.ComparisonPage.Component'));
const Toplist = React.lazy(() => import('./component/Toplist.ComparisonPage.Component'));

const initialize_pie_data = {
    labels: [],
    data: [],
}
class ComparisonPage extends Component {
    constructor(props) {

        super(props);
        this.state = {
            is_bar_chart: false
        }
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true });
        this.hideLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: false });
    }

    componentDidMount() {
        const { dispatch, child_route, f7route } = this.props;
        dispatch({ type: setGlobalParam, storage: 'comparison_route_key', response: child_route });
        dispatch({ type: setGlobalParam, storage: 'comparison_pie_data', response: initialize_pie_data })
        dispatch(getPageDataEvent({ page: f7route.path }));
        // this.showLoading();
    }

    componentDidUpdate(prevProps) {
        const { date, chart_limit, comparison_pie_data, comparison_route_key, dispatch, f7route } = this.props;

        if (comparison_route_key != prevProps.comparison_route_key) {
            this.showLoading();
        }

        if (chart_limit !== prevProps.chart_limit) {
            dispatch(getPageDataEvent({ page: f7route.path }));
        }

        if (date != prevProps.date) {
            dispatch(getPageDataEvent({ page: f7route.path }));
        }

        if (comparison_pie_data != prevProps.comparison_pie_data && comparison_pie_data != initialize_pie_data) {
            this.hideLoading();
        }
    }
    // componentWillReceiveProps(nextProps) {
    //     const { comparison_pie_data } = this.props;
    //     if (comparison_pie_data != nextProps.comparison_pie_data && comparison_pie_data != initialize_pie_data) {
    //         this.hideLoading();
    //     }
    // }

    handleToggle(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    loadMore(event, done) {
        const { dispatch, f7route } = this.props;
        dispatch(getPageDataEvent({ page: f7route.path }));
        setTimeout(() => {
            done();
        }, 1000);
    }

    render() {
        const { is_bar_chart } = this.state;
        const {
            child_route,
            date_from,
            date_to,
        } = this.props;
        const is_show_divison = localStorage.getItem('is_show_divison');

        return (
            <Page id='comparisonPage' name='ComparisonPage' className='comparison' ptr onPtrRefresh={this.loadMore.bind(this)} >
                <Navbar>
                    <NavLeft>
                        <Link iconIos="f7:menu" iconMd="material:menu" panelOpen="left"></Link>
                    </NavLeft>
                    <NavTitle style={{ fontSize: '16px' }} >{`${child_route} Comparison`}</NavTitle>
                </Navbar>

                <SelectDateComponent />
                <div className="accordion-toggle">
                    <Icon material="insert_chart"></Icon>
                    <Toggle name='is_bar_chart' checked={is_bar_chart} className="no-fastclick" onChange={this.handleToggle.bind(this)} color="blue" />
                    <Icon material="pie_chart"></Icon>
                </div>
                <List>
                    {
                        is_show_divison == 'true' ?
                            <Suspense fallback={<div>Loading...</div>}>
                                <DivisionListComponent />
                            </Suspense> : ''
                    }
                    <Suspense fallback={<div>Loading...</div>}>
                        <Toplist />
                    </Suspense>
                </List>
                <Suspense fallback={<div>Loading...</div>}>
                    <Chart dateFrom={date_from} dateTo={date_to} is_bar_chart={is_bar_chart} />
                </Suspense>

            </Page>
        );
    }

}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ComparisonPage);
