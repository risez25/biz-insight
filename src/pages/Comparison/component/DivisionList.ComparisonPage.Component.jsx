import Dom7 from 'dom7';
import { ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';
import { getEvent, getRefreshEvent } from './../../../../../global/global.Saga';

class DivisionList extends PureComponent {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
    }

    useSelectDivsion(division_id) {
        const { dispatch } = this.props;
        dispatch({ type: setGlobalParam, storage: 'selected_division_id', response: division_id });
        dispatch(getRefreshEvent());
        
        this.showLoading();
        var $$ = Dom7;
        $$('#salesteam .item-after').html('');
        $$('#salesteam option[value=""]').prop('selected', true);
    }

    render() {
        const {
            divisionlist_state,
        } = this.props;

        return (
            <ul>
                <ListItem
                    title="Divsions"
                    smartSelect
                    id="divisions"
                    smartSelectParams={{ openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search Division', closeOnSelect: true }}
                >
                    <select name="divisions" onChange={(e) => this.useSelectDivsion(e.target.options.selectedIndex)}>
                        <option value="" >All</option>
                        {
                            divisionlist_state ?
                                divisionlist_state.map((value, index) => {
                                    return (<option key={index} value={value.id}>{value.code}</option>)
                                }) : ''}
                    </select>
                </ListItem>
            </ul>
        );
    }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(DivisionList);
