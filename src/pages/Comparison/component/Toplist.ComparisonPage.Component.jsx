import 'flatpickr/dist/themes/material_green.css';
import { List, ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../global/global.Reducer';

class TopListComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            chart_limit: 20
        }
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true });
    }

    onSelectTopList(value) {
        const { dispatch } = this.props;
        dispatch({ type: setGlobalParam, storage: 'chart_limit', response: value });
        this.showLoading();
    }
    render() {
        const {
            comparison_pie_data,
        } = this.props;

        let labels = comparison_pie_data.labels;

        return (
            <ul>
                <ListItem
                    title="Top List"
                    smartSelect
                    smartSelectParams={{ openIn: 'popover', closeOnSelect: true }}
                >
                    <select name="toplist" onChange={(e) => this.onSelectTopList(e.target.value)} defaultValue={'20'}>
                        <option key={1} value="10">10</option>
                        <option key={2} value="20">20</option>
                        <option key={3} value="30">30</option>
                        <option key={4} value="40">40</option>
                        <option key={5} value="50">50</option>
                    </select>
                </ListItem>
            </ul>
        );
    }

}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(TopListComponent);
