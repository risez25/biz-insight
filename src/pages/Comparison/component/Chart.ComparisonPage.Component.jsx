import 'flatpickr/dist/themes/material_green.css';
import {  List, ListItem,  Row, Col } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { Pie, Bar } from 'react-chartjs-2';

const backgroundColor = [
    "#388e3c",
    "#b85800",
    "#391446",
    "#1e2bf2",
    "#7e55f4",
    "#78dde0",
    "#7e87e1",
    "#e2b1ed",
    "#08990d",
    "#0940fd",
    "#646a0a",
    "#fa71f7",
    "#d7928c",
    "#c5102b",
    "#f02327",
    "#2954e5",
    "#0e32e9",
    "#a7da39",
    "#26e9f6",
    "#b8c30a",
    "#c6871f",
    "#d3d05e",
    "#f54edd",
    "#9451bb",
    "#bdec6c",
    "#e8746d",
    "#451de6",
    "#126afd",
    "#59279c",
    "#b29198",
    "#e1b7fc", // 31
    "#3d6327",
    "#489a6d",
    "#d79194",
    "#a57a4f",
    "#d2def6",
    "#826b47",
    "#859228",
    "#d2472b",
    "#439c48",
    "#56e8f2", // 41
    "#18caba",
    "#2e22b6",
    "#e7fa51",
    "#bc3cb0",
    "#5bfff7",
    "#5170d8",
    "#916292",
    "#bfcb35",
    "#bf4d51"
];
class ChartComparisonComponent extends PureComponent {
    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    updateDataset(e, datasetIndex) {
        const { is_bar_chart } = this.props;
        if (is_bar_chart) {//is_bar_chart
            let ci = this.refs.chart.chartInstance;
            let meta = ci.getDatasetMeta(0);
            meta.data[datasetIndex].hidden = !meta.data[datasetIndex].hidden;
            ci.update();
        } else {
            let ci = this.refs.chart.chartInstance;
            let meta = ci.getDatasetMeta(datasetIndex);
            meta.hidden = !meta.hidden
            ci.update();
        }
    };

    render() {
        const {
            is_bar_chart,
            comparison_pie_data,
        } = this.props;


        let labels = comparison_pie_data.labels;
        let bar_chart_data = comparison_pie_data.data.map((value, index) => {
            return {
                label: labels[index],
                backgroundColor: backgroundColor[index],
                data: [value]
            }
        });

        return (
            <div>
                <List style={{ marginBottom: "-40px" }} >
                    <Row noGap>
                        <Col width="60">
                            {!is_bar_chart ?
                                <Bar
                                    className='bar-chart'
                                    ref="chart"
                                    height={300}
                                    legend={{ display: false }}
                                    options={{
                                        responsive: true,
                                        tooltips: {
                                            callbacks: {
                                                label: (t, d) => {
                                                    return d.datasets[t.datasetIndex].label + " " + this.numberFormat(d.datasets[t.datasetIndex].data[0]);
                                                }
                                            }
                                        },
                                    }}
                                    data={{
                                        datasets: bar_chart_data
                                    }}
                                />
                                :
                                <Pie
                                    className='pie-chart'
                                    ref="chart"
                                    height={300}
                                    legend={{ display: false }}
                                    options={{
                                        responsive: true,
                                        tooltips: {
                                            callbacks: {
                                                label: (t, d) => {
                                                    return d.labels[t.index] + " " + this.numberFormat(d.datasets[0].data[t.index]);
                                                }
                                            }
                                        },
                                    }}
                                    data={{
                                        labels: comparison_pie_data.labels,
                                        datasets: [{
                                            data: comparison_pie_data.data,
                                            backgroundColor: backgroundColor,
                                            hoverBackgroundColor: backgroundColor
                                        }]
                                    }} />}
                        </Col>
                        <Col width="40"  >
                            <div >
                                <List style={{ overflow: 'scroll', position: 'absolute', height: '90%', marginTop: 'unset' }} >
                                    {comparison_pie_data.data.map((value, index) => {
                                        let patt = new RegExp('^[^-]+');
                                        let firstWord = patt.exec(labels[index]);
                                        return (
                                            <ListItem key={index} name={value}>
                                                <div className="item-title-comparison" slot="title">{firstWord}</div>
                                                <div className="item-media" slot="media" style={{ background: backgroundColor[index], minWidth: 18, marginLeft: "-10px" }} ></div>
                                                {/* <Label align='right' slot="after" >{this.numberFormat(value)}</Label> */}
                                            </ListItem>
                                        )
                                    })}
                                </List>
                            </div>
                        </Col>

                    </Row>
                </List>
                <List >
                    {comparison_pie_data.data.map((value, index) => {
                        return (
                            <ListItem key={index} checkbox defaultChecked name={value} onClick={(e) => this.updateDataset(e, index)}>
                                <div className="item-title-comparison" slot="title">{labels[index]}</div>
                                <div className="item-media" slot="media" style={{ background: backgroundColor[index], minWidth: 18, marginLeft: "-10px" }} ></div>
                                <p align='right' slot="after" >{this.numberFormat(value)}</p>
                            </ListItem>
                        )
                    })}
                </List>
            </div>
        );
    }

}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ChartComparisonComponent);
