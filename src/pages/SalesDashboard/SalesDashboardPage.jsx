import Dom7 from 'dom7';
import 'flatpickr/dist/themes/material_green.css';
import { Block, Card, CardContent, Col, Label, Link, List, ListItem, Navbar, NavLeft, NavTitle, Page, Row, Button, NavRight } from 'framework7-react';
import moment from 'moment';
import React, { PureComponent, lazy, Suspense } from 'react';
import { connect } from "react-redux";
import { setGlobalParam, setSalesPageToDefault } from '../../global/global.Reducer';
import { getPageDataEvent } from './../../global/global.Saga';

const SelectDateComponent = React.lazy(() => import('../../components/selectDate.Component'));
const AccordionComponent = React.lazy(() => import('./component/accordion/Accordion.SalesPage.Component'));
const SalesCardComponent = React.lazy(() => import('./component/card/SalesCard.SalesPage.Component'));
const SmartSelect = React.lazy(() => import('./component/smartselect/SmartSelect.SalesPage.Component'));

class SalesDasboardPage extends PureComponent {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
    this.hideLoadign = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: false });

  }

  componentDidMount() {
    const { dispatch, isAuthenticated, f7route } = this.props;
    if (isAuthenticated) {
      dispatch(getPageDataEvent({ page: f7route.path }));
    }
  }
  componentDidUpdate(prevProps) {
    const { dispatch, isAuthenticated, sales_target_obj, f7route } = this.props;
    if (isAuthenticated != prevProps.isAuthenticated && isAuthenticated) {
      dispatch(getPageDataEvent({ page: f7route.path }));
    }
    if (sales_target_obj == {} && isAuthenticated) {
      dispatch(getPageDataEvent({ page: f7route.path }));
    }
  }

  usePullToRefresh(event, done) {
    const { dispatch, f7route } = this.props;
    dispatch(getPageDataEvent({ page: f7route.path }));
    setTimeout(() => {
      done();
    }, 1000);
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: setSalesPageToDefault });
  }

  render() {
    return (
      <Page id='HomePage' name='HomePage' className='home' ptr onPtrRefresh={this.usePullToRefresh.bind(this)} >
        <Navbar>
          <NavLeft>
            <Link iconIos="f7:menu" iconMd="material:menu" panelOpen="left"></Link>
          </NavLeft>
          <NavTitle>Sales Dashboard</NavTitle>
        </Navbar>

        <Suspense fallback={<div>Loading...</div>}>
          <SelectDateComponent />
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <SmartSelect />
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <SalesCardComponent />
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <AccordionComponent />
        </Suspense>
      </Page >
    );
  }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesDasboardPage);
