import { Col, Label, Row, Card, CardContent } from "framework7-react";
import moment from 'moment';
import React, { Component } from "react";
import { connect } from "react-redux";
import collection from "../../../../image/collection.png";
import returns from "../../../../image/returns.png";
import sales from "../../../../image/sales.png";
import visit from "../../../../image/visit.png";
import { setGlobalParam } from "../../../../global/global.Reducer";

class AccordionDate extends Component {
    constructor(props) {
        super(props)

        this.myRef = React.createRef();
        this.sumAmount = this.sumAmount.bind(this);
        const { dispatch } = this.props;
        this.hideLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: false });
    }


    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    sumAmount(object, storage_name) {
        const { dispatch, date_type, date_from } = this.props;
        let data = 0;
        let idx = 0;

        switch (date_type) {
            case 'year':
                Object.keys(object).map((key) => {
                    idx++;
                    if (typeof object[key] == 'number') {
                        data = parseFloat(data) + parseFloat(object[key]);
                    }
                });
                break;
            case 'month':
                Object.keys(object).map((key) => {
                    idx++;
                    if (typeof object[key] == 'number') {
                        data = parseFloat(data) + parseFloat(object[key]);
                    }
                });
                break;
            case 'day':
                console.log(object);

                let day = moment(date_from).format('dddd');
                idx = 2;
                data = object[day];
                break;
            default:
                break;
        }


        if (storage_name == 'total_sales_amt') {
            if (idx > 1 || data == NaN || data == undefined) {
                this.hideLoading();
            }
        }

        data = data == NaN || data == undefined ? 0 : data.toFixed(2);
        dispatch({ type: setGlobalParam, storage: storage_name, response: data });
    }

    sumTarget(object, storage_name) {
        const { dispatch, date_type, date_from } = this.props;
        let data = 0
        switch (date_type) {
            case 'year':
                Object.keys(object).map((key) => {
                    if (typeof object[key] == 'number') {
                        data = parseFloat(data) + parseFloat(object[key]);
                    }
                });
                break;
            case 'month':
                data = object[moment(date_from).format('MMM')];
                break;
            default:
                break;
        }
        data = data == NaN || data == undefined ? 0 : data;
        dispatch({ type: setGlobalParam, storage: storage_name, response: data })
    }

    openAccordion(accordion_name) {
        const { dispatch, is_sales_accordion_open, is_collection_accoridon_open, is_return_accordion_open } = this.props;

        switch (accordion_name) {
            case 'sales':
                dispatch({ type: setGlobalParam, storage: 'is_sales_accordion_open', response: !is_sales_accordion_open });
                dispatch({ type: setGlobalParam, storage: 'is_collection_accoridon_open', response: false });
                dispatch({ type: setGlobalParam, storage: 'is_return_accordion_open', response: false });
                break;
            case 'collect':
                dispatch({ type: setGlobalParam, storage: 'is_sales_accordion_open', response: false });
                dispatch({ type: setGlobalParam, storage: 'is_collection_accoridon_open', response: !is_collection_accoridon_open });
                dispatch({ type: setGlobalParam, storage: 'is_return_accordion_open', response: false });
                break;
            case 'return':
                dispatch({ type: setGlobalParam, storage: 'is_sales_accordion_open', response: false });
                dispatch({ type: setGlobalParam, storage: 'is_collection_accoridon_open', response: false });
                dispatch({ type: setGlobalParam, storage: 'is_return_accordion_open', response: !is_return_accordion_open });
                break;
            default:
                break;
        }
    }

    componentDidUpdate(prevProps) {
        const {
            isAuthenticated,
            //sales param
            sales_accordion_data,
            //collection param
            collection_accordion_data,
            //return param
            return_accordion_data,
        } = this.props;


        //get total amount
        if (isAuthenticated) {
            if (sales_accordion_data != null && sales_accordion_data != [] && sales_accordion_data != prevProps.sales_accordion_data) {
                this.sumAmount(sales_accordion_data.inv, 'total_inv_amt');
                this.sumAmount(sales_accordion_data.so, 'total_sales_amt');
            }
            if (collection_accordion_data != null && collection_accordion_data != [] && collection_accordion_data != prevProps.collection_accordion_data) {
                this.sumAmount(collection_accordion_data, 'total_collection_amt');
            }
            if (return_accordion_data != null && return_accordion_data != [] && return_accordion_data != prevProps.return_accordion_data) {
                this.sumAmount(return_accordion_data, 'total_return_amt');
            }
        }

    }

    handleToggle(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const {
            total_sales_amt, total_inv_amt, total_collection_amt, total_return_amt,
            total_visit_outlet,
            date_type,
        } = this.props;
        return (
            <Row>
                <Col id="salesCard" width="50" tabletWidth="25">
                    <div onClick={() => this.openAccordion('sales')}>
                        <Card className="sales-page-card button" style={{ backgroundColor: '#20A8D8' }}>
                            <CardContent>
                                <Row>
                                    <Col width="25">
                                        <img src={sales} className="card-icon" alt="Image not found" />
                                    </Col>
                                    <Col width="75">
                                        {this.numberFormat((parseFloat(total_sales_amt) + parseFloat(total_inv_amt)))}
                                        <Label>Sales</Label>
                                    </Col>
                                </Row>
                            </CardContent>
                        </Card>
                    </div>
                </Col>
                <Col id="collectionCard" width="50" tabletWidth="25">
                    <div onClick={() => this.openAccordion('collect')}>
                        <Card className="sales-page-card button" style={{ backgroundColor: '#FFC107' }}>
                            <CardContent>
                                <Row>
                                    <Col width="25">
                                        <img src={collection} className="card-icon" />
                                    </Col>
                                    <Col width="75">
                                        {this.numberFormat(total_collection_amt)}
                                        <Label>Collection</Label>
                                    </Col>
                                </Row>
                            </CardContent>
                        </Card>
                    </div>
                </Col>
                <Col id="returnCard" width="50" tabletWidth="25">
                    <div onClick={() => this.openAccordion('return')}>
                        <Card className="sales-page-card button" style={{ backgroundColor: '#F86C6B' }}>
                            <CardContent>
                                <Row>
                                    <Col width="25">
                                        <img src={returns} className="card-icon" />
                                    </Col>
                                    <Col width="75">
                                        {this.numberFormat(total_return_amt)}
                                        <Label>Return</Label>
                                    </Col>
                                </Row>
                            </CardContent>
                        </Card>
                    </div>
                </Col>
                <Col id="visitCard" width="50" tabletWidth="25">
                    <Card className="sales-page-card button" style={{ backgroundColor: '#63C2DE' }}>
                        <CardContent>
                            <Row>
                                <Col width="25">
                                    <img src={visit} className="card-icon" />
                                </Col>
                                <Col width="75">
                                    {date_type != 'day' ? `${total_visit_outlet.total_visit}/${total_visit_outlet.total_outlet}` : `${total_visit_outlet.total_visit}`}
                                    <Label>Total Visit</Label>
                                </Col>
                            </Row>
                        </CardContent>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(AccordionDate);
