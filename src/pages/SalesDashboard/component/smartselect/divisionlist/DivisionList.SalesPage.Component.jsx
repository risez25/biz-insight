import Dom7 from 'dom7';
import { ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';
import { getEvent, getPageDataEvent } from './../../../../../global/global.Saga';

class DivisionList extends PureComponent {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
        this.hideLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: false });
    }
    componentDidMount(){
        const {dispatch} =this.props;
        dispatch(getEvent)
    }

    useSelectDivsion(division_id) {
        const { dispatch, f7route } = this.props;
        dispatch({ type: setGlobalParam, storage: 'selected_division_id', response: division_id });
        dispatch(getEvent({ api: `bizInsight/salesmanTeamList&user_id=${sales_id}&role=${role}&division_id=${division_id}`, storage: 'salesman_team_list' }));
        dispatch(getEvent({ api: `bizInsight/getSalesmanVisitAndOutlet&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}`, storage: 'total_visit_outlet' }));
        switch (date_type) {
            case 'day':
                let date_start_week = moment(date_from).startOf('isoWeek').format('YYYY-MM-DD');
                let date_end_week = moment(date_to).endOf('isoWeek').format('YYYY-MM-DD');
                dispatch(getEvent({ api: `bizInsight/getSalesByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${division_id}`, storage: 'sales_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getCollectionByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${division_id}`, storage: 'collection_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getReturnByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start_week}&date_to=${date_end_week}&division_id=${division_id}`, storage: 'return_accordion_data' }));
                break;
            case 'month':
                dispatch(getEvent({ api: `bizInsight/getSalesByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'sales_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getCollectionByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'collection_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getReturnByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'return_accordion_data' }));
                break;
            case 'year':
                dispatch(getEvent({ api: `bizInsight/getSalesByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'sales_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getCollectionByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'collection_accordion_data' }));
                dispatch(getEvent({ api: `bizInsight/getReturnByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${division_id}`, storage: 'return_accordion_data' }));
                break;
            default:
                break;
        }
        // dispatch(getPageDataEvent({ page: f7route.path }));
        
        this.showLoading();
        var $$ = Dom7;
        $$('#salesteam .item-after').html('');
        $$('#salesteam option[value=""]').prop('selected', true);
    }

    render() {
        const {
            divisionlist_state,
        } = this.props;

        return (
            <ul>
                <ListItem
                    title="Divsions"
                    smartSelect
                    id="divisions"
                    smartSelectParams={{ openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search Division', closeOnSelect: true }}
                >
                    <select name="divisions" onChange={(e) => this.useSelectDivsion(e.target.options.selectedIndex)}>
                        <option value="" >All</option>
                        {
                            divisionlist_state ?
                                divisionlist_state.map((value, index) => {
                                    return (<option key={index} value={value.id}>{value.code}</option>)
                                }) : ''}
                    </select>
                </ListItem>
            </ul>
        );
    }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(DivisionList);
