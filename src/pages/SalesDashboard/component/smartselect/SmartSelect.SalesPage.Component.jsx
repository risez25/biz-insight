import 'flatpickr/dist/themes/material_green.css';
import { Block, Card, CardContent, Col, Label, Link, List, ListItem, Navbar, NavLeft, NavTitle, Page, Row, Button } from 'framework7-react';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../global/global.Reducer';
import { getEvent } from './../../../../global/global.Saga';
import SalesTeamList from './salesteamlist/SalesTeamList.SalesPage.Component';
import SalesmanList from './salesmanlist/SalesmanList.SalesPage.Componnet';
import DivisionListComponent from '../../../../components/DivisionList.Component';


class SmartSelect extends PureComponent {
    componentDidUpdate(prevProps) {
        const { is_salesteam_change, date_from, date_to, date_type, sales_id, dispatch, isAuthenticated, date, selected_division_id } = this.props;

        if (
            (is_salesteam_change != prevProps.is_salesteam_change ||
                sales_id != prevProps.sales_id ||
                date_type != prevProps.date_type ||
                date != prevProps.date) &&
            isAuthenticated
        ) {
            dispatch(getEvent({ api: `bizInsight/getSalesmanVisitAndOutlet&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}`, storage: 'total_visit_outlet' }));
            switch (date_type) {
                case 'day':
                    let date_start = moment(date_from).startOf('isoWeek').format('YYYY-MM-DD');
                    let date_end = moment(date_to).endOf('isoWeek').format('YYYY-MM-DD');
                    dispatch(getEvent({ api: `bizInsight/getSalesByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start}&date_to=${date_end}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getCollectionByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start}&date_to=${date_end}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getReturnByDay&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start}&date_to=${date_end}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                    break;
                case 'month':
                    dispatch(getEvent({ api: `bizInsight/getSalesByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getCollectionByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getReturnByMonth&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                    break;
                case 'year':
                    dispatch(getEvent({ api: `bizInsight/getSalesByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'sales_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getCollectionByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'collection_accordion_data' }));
                    dispatch(getEvent({ api: `bizInsight/getReturnByYear&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_from}&date_to=${date_to}&division_id=${selected_division_id}`, storage: 'return_accordion_data' }));
                    break;
                default:
                    break;
            }
        }

        if (
            (is_salesteam_change != prevProps.is_salesteam_change
                || sales_id != prevProps.sales_id
                || isAuthenticated != prevProps.isAuthenticated
                || date != prevProps.date) &&
            isAuthenticated
        ) {
            let date_start = moment(date_from).startOf('year').format('YYYY-MM-DD');
            let date_end = moment(date_to).endOf('year').format('YYYY-MM-DD');
            dispatch(getEvent({ api: `bizInsight/GetSalesTarget&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start}&date_to=${date_end}`, storage: 'sales_target_obj' }));
            dispatch(getEvent({ api: `bizInsight/GetCollectionTarget&is_salesteam=${is_salesteam_change}&id=${sales_id}&date_from=${date_start}&date_to=${date_end}`, storage: 'collection_target_obj' }));
        }
    }

    render() {
        const {
            role,
            username
        } = this.props;
        const is_show_divison = localStorage.getItem('is_show_divison');
        return (
            <>
                {role != 'salesman' ?
                    <List style={{ marginBottom: 6 }} >
                        {
                            is_show_divison == 'true' ? <DivisionListComponent changeDOM={{ html: '#salesteam .item-after', prop: '#salesteam option[value=""]' }} /> : ''
                        }
                        <SalesTeamList />
                        <SalesmanList />
                    </List>
                    : <Block style={{ textAlign: 'center' }} ><p>{username}</p></Block>}

            </>
        );
    }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SmartSelect);
