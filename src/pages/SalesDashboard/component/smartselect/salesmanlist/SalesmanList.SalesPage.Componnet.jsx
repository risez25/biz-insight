import 'flatpickr/dist/themes/material_green.css';
import { ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class SalesmanList extends PureComponent {
    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
    }

    onSelectSalesman(sales_id) {
        const { dispatch } = this.props;
        dispatch({ type: setGlobalParam, storage: 'is_salesteam_change', response: false });
        dispatch({ type: setGlobalParam, storage: 'sales_id', response: sales_id });
        this.showLoading();
    }

    render() {
        const {
            salesman_team_list,
            is_salesteam_change,
            sales_team_index
        } = this.props;

        return (
            <ul>
                <ListItem
                    title="Salesman"
                    smartSelect
                    id="salesman"
                    smartSelectParams={{ openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search Salesman', closeOnSelect: true, cssClass: "navbar-smartselect" }}
                >
                    <select name="salesman" onChange={(e) => this.onSelectSalesman(e.target.value)}>
                        <option value="" checked={is_salesteam_change}>All</option>
                        {salesman_team_list[sales_team_index] ?
                            salesman_team_list[sales_team_index]['salesteam_member'].map((value, index) => {
                                return (<option key={index} value={value.value}>{value.label}</option>)
                            }) : ''}

                    </select>
                </ListItem>
            </ul>
        );
    }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesmanList);
