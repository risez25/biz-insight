import Dom7 from 'dom7';
import 'flatpickr/dist/themes/material_green.css';
import { ListItem } from 'framework7-react';
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class SalesTeamList extends PureComponent {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true })
  }

  useFindSalesteamIndex(salesman_team_list) {
    const { dispatch, salesteam_id } = this.props;
    let index = 0;
    salesman_team_list.map((value, idx) => {
      if (value.id == salesteam_id) {
        index = idx;
        var $$ = Dom7;
        $$('#salesteam .item-after').html(value.code);
      }
    })
    dispatch({ type: setGlobalParam, storage: 'sales_team_index', response: index });
  }

  componentWillReceiveProps(nextProps) {
    const { salesman_team_list, role } = this.props;
    if (salesman_team_list != nextProps.salesman_team_list) {
      if (role == 'salesmanager') {
        this.useFindSalesteamIndex(nextProps.salesman_team_list);
      }
    }
  }

  onSelectSalesman(sales_id) {
    const { dispatch } = this.props;
    dispatch({ type: setGlobalParam, storage: 'is_salesteam_change', response: false });
    dispatch({ type: setGlobalParam, storage: 'sales_id', response: sales_id });
    this.showLoading();
  }

  onSelectSalesmanTeam(index) {
    const { dispatch, salesman_team_list } = this.props;
    dispatch({ type: setGlobalParam, storage: 'sales_team_index', response: index });
    dispatch({ type: setGlobalParam, storage: 'is_salesteam_change', response: true });
    dispatch({ type: setGlobalParam, storage: 'sales_id', response: salesman_team_list[index].id });
    var $$ = Dom7;
    $$('#salesman .item-after').html('All');
    $$('#salesman option[value=""]').prop('selected', true);
    this.showLoading();
  }

  render() {
    const {
      salesman_team_list,
      role
    } = this.props;

    return (
      <ul>
        <ListItem
          title="Sales Team"
          smartSelect
          id="salesteam"
          // disabled
          smartSelectParams={{ openIn: 'popup', searchbar: true, searchbarPlaceholder: 'Search SalesTeam', closeOnSelect: true }}
        >
          <select name="salesmanTeam" onChange={(e) => this.onSelectSalesmanTeam(e.target.options.selectedIndex)}>
            {
              salesman_team_list ?
                salesman_team_list.map((value, index) => {
                  return (<option key={index} value={value.id}>{value.code}</option>)
                }) : ''}
          </select>
        </ListItem>
      </ul>
    );
  }

}


export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesTeamList);
