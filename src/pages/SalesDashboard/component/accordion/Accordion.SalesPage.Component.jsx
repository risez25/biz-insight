import React, { Component, lazy, Suspense } from "react";
import { connect } from "react-redux";
const SalesAccordion = React.lazy(() => import('./sales/SalesAccordion.SalesPage.Component'));
const ReturnAccordion = React.lazy(() => import('./return/ReturnAccordion.SalesPage.Component'));
const CollectionAccordion = React.lazy(() => import('./collection/CollectionAccordion.SalesPage.Component'));

class AccordionDate extends Component {
    render() {
        const {
            is_sales_accordion_open, is_collection_accoridon_open, is_return_accordion_open
        } = this.props;
        return (
            <div className="list" style={{ marginTop: 6 }}>
                <ul>
                    <li id="salesAccordion" className={`accordion-item ${is_sales_accordion_open ? 'accordion-item-opened' : ''}`} >
                        <Suspense fallback={<div>Loading...</div>}>
                            <SalesAccordion />
                        </Suspense>
                    </li>
                    <li id="collectionAccordion" className={`accordion-item ${is_collection_accoridon_open ? 'accordion-item-opened' : ''}`}>
                        <Suspense fallback={<div>Loading...</div>}>
                            <CollectionAccordion />
                        </Suspense>
                    </li>
                    <li id="returnAccordion" className={`accordion-item ${is_return_accordion_open ? 'accordion-item-opened' : ''}`}>
                        <Suspense fallback={<div>Loading...</div>}>
                            <ReturnAccordion />
                        </Suspense>
                    </li>
                </ul>
            </div>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(AccordionDate);
