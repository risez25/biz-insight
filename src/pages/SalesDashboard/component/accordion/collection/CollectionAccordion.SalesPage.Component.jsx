import { Col, Label, Progressbar, Row, Tab, Tabs } from "framework7-react";
import moment from 'moment';
import React, { Component } from "react";
import { connect } from "react-redux";
import CollectionToggle from "./CollectionToggle.SalesPage.Component";
import CollectionTabList from "./CollectionTabList.SalesPage.Component";
import CollectionTabChart from "./CollectionTabChart.SalesPage.Component";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class CollectionAccordion extends Component {
    constructor(props) {
        super(props)
        this.sumTarget = this.sumTarget.bind(this);
    }


    sumTarget() {
        const { dispatch, date_type, date_from, collection_target_obj } = this.props;
        let data = 0
        switch (date_type) {
            case 'year':
                Object.keys(collection_target_obj).map((key) => {
                    if (typeof collection_target_obj[key] == 'number') {
                        data = parseFloat(data) + parseFloat(collection_target_obj[key]);
                    }
                });
                break;
            case 'month':
                data = collection_target_obj[moment(date_from).format('MMM')];
                break;
            default:
                break;
        }
        data = data == NaN || data == undefined ? 0 : data;
        dispatch({ type: setGlobalParam, storage: 'total_collection_target', response: data })
    }

    componentDidUpdate(prevProps) {
        const {
            collection_target_obj, total_collection_amt, total_collection_target,
            dispatch, date_type, date
        } = this.props;

        // get total target
        if (
            collection_target_obj != null && collection_target_obj != {} && collection_target_obj != prevProps.collection_target_obj
            || date_type != prevProps.date_type
            || date != prevProps.date
        ) {
            this.sumTarget();
        }

        //get total percentage
        if (total_collection_target != 0 || total_collection_target != prevProps.total_collection_target) {
            dispatch({ type: setGlobalParam, storage: 'total_collection_percentage', response: parseFloat(((total_collection_amt / total_collection_target) * 100).toFixed(2)) })
        }
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const {
            total_collection_percentage, total_collection_amt, total_collection_target,
            date_type, collection_toggle
        } = this.props;
        return (
            <>
                <a href="#" className="item-content item-link" style={{ fontSize: 12, paddingLeft: 0 }}><div className="item-inner" style={{ backgroundColor: '#E5E5E5' }}>
                    <div className="item-title" style={{ minHeight: 65 }}>
                        <Row>
                            <Col width={100}>
                                {
                                    date_type != 'day' ?
                                        <div>
                                            <p className="item-title-month">Collection</p>
                                            <div style={{ marginTop: '-35px', paddingLeft: 16 }}>
                                                <p style={{ marginBottom: '-7px', textAlign: 'right', top: 7, fontSize: 12 }}>{"Target: " + this.numberFormat(total_collection_target)}</p>
                                                <Progressbar color="yellow" style={{ top: 10 }} progress={parseFloat(total_collection_percentage) > 100 ? 100 : parseFloat(total_collection_percentage)}></Progressbar>
                                                <p style={{ marginBottom: '-7px', textAlign: 'left', top: 15, fontSize: 12 }}>{this.numberFormat(total_collection_amt)}</p>
                                            </div>
                                        </div>
                                        :
                                        <p className="item-title-day">Collection</p>
                                }
                            </Col>
                        </Row>
                    </div>
                    <div className="item-after">
                        {date_type != 'day' ?
                            <Label>{parseFloat(total_collection_percentage) > 100 ? '100%' : parseFloat(total_collection_percentage) + '%'}</Label> :
                            <p className="item-after-amt">{this.numberFormat(total_collection_amt)}</p>
                        }
                    </div>
                </div></a>
                <div className="accordion-item-content">
                    <div className="block">
                        <CollectionToggle />
                        <Tabs>
                            <Tab id="tab-1" className={`${collection_toggle ? 'tab-active' : ''}`} >
                                <CollectionTabList />
                            </Tab>
                            <Tab id="tab-2" className={`${collection_toggle ? '' : 'tab-active'}`}>
                                <CollectionTabChart />
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(CollectionAccordion);
