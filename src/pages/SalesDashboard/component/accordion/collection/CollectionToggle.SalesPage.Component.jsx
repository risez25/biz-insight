import { Icon, Toggle } from "framework7-react";
import React, { Component } from "react";
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class CollectionToggle extends Component {
    handleToggle(event) {
        const { dispatch, collection_toggle } = this.props;
        dispatch({ type: setGlobalParam, storage: 'collection_toggle', response: !collection_toggle })
    }

    render() {
        const {
            collection_toggle
        } = this.props;
        return (
            <div className="accordion-toggle">
                <Icon material="insert_chart"></Icon>
                <Toggle name='collection_toggle' checked={collection_toggle} className="no-fastclick" onChange={(e)=>this.handleToggle(this)} color="blue" />
                <Icon material="list"></Icon>
            </div>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(CollectionToggle);
