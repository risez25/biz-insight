import { Block, Col, Icon, Label, ListItem, Progressbar, Row, Tab, Tabs, Toggle } from "framework7-react";
import moment from 'moment';
import React, { Component } from "react";
import { Bar } from 'react-chartjs-2';
import { connect } from "react-redux";
import { setGlobalParam } from "../../../../../global/global.Reducer";
import ReturnToggle from "./ReturnToggle.SalesPage.Component";
const date_format = 'DD/MM/YY';

class ReturnTabList extends Component {
    dateDayList() {
        const { date_from } = this.props;

        let date = moment(date_from);
        let date_start = moment(date.startOf('isoWeek'));
        let date_end = moment(date.endOf('isoWeek'));

        let date_list = [];
        while (date_start < date_end) {
            let date_array = { from: '', to: '' };
            date_array.from = moment(date_start).format('DD/MM');
            date_array.to = moment(date_start).format('DD/MM');
            date_start = moment(date_start).add(1, 'day');
            date_list.push(date_array);
        }

        return date_list

    }

    dateMonthList() {
        const { date_from, date_to } = this.props;
        let from = moment(date_from);
        let to = moment(date_to);
        let date_index = from;
        let date_list = [];
        while (date_index < to) {
            let date_array = { from: '', to: '' };
            date_array.from = moment(date_index).format('DD/MM');
            date_index = moment(date_index).add(1, 'week').startOf('isoWeek');
            date_array.to = moment(date_index).subtract(1, 'day').format('DD/MM');
            date_list.push(date_array);
        }
        date_list[date_list.length - 1].to = moment(date_to).format('DD/MM');
        return date_list;
    }

    renderList() {
        const {
            date_type,
            return_accordion_data, return_target_obj,
        } = this.props;
        let { date_footer, date_list, list, accordion_data, sales_data, target_obj } = ''

        accordion_data = return_accordion_data;
        target_obj = return_target_obj;


        switch (date_type) {
            case 'day':
                date_list = this.dateDayList();
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    date_footer = date_list[index] ? date_list[index]['from'] : '';
                    return (
                        <ListItem key={index} title={key} footer={date_footer}>
                            <div slot="after">{this.numberFormat(accordion_data[key])}</div>
                        </ListItem>
                    )
                }) : "don't have data";
                break;
            case 'month':
                date_list = this.dateMonthList();
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    date_footer = date_list[index] ? date_list[index]['from'] + ' - ' + date_list[index]['to'] : '';

                    return (
                        <ListItem key={index} title={key} footer={date_footer}
                            after={this.numberFormat(accordion_data[key])} />
                    )
                }) : "don't have data";
                break;
            case 'year':
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    return (
                        <li key={index} >
                            <div className="item-content" style={{ fontSize: 12, paddingLeft: 0 }}>
                                <div className="item-inner" style={{ paddingTop: 0, paddingBottom: 0, marginBottom: '-10px' }}>
                                    <div className="item-title" style={{ minHeight: 65 }}>
                                        <p style={{ paddingTop: 13, fontSize: 14 }}>{key}</p>
                                    </div>
                                    <div className="item-after">
                                        {this.numberFormat(accordion_data[key])}
                                    </div>
                                </div>
                            </div>
                        </li>
                    );
                }) : "don't have data";
                break;
            default:
                break;
        }
        return list;
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const return_list = this.renderList();
        return (
            <Block>
                <ul>
                    {return_list}
                </ul>
            </Block>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ReturnTabList);
