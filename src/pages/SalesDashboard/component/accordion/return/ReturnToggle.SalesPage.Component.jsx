import { Icon, Toggle } from "framework7-react";
import React, { Component } from "react";
import { connect } from "react-redux";
import { setGlobalParam } from "../../../../../global/global.Reducer";

class ReturnAccordion extends Component {
    handleToggle(event) {
        const { dispatch, return_toggle } = this.props;
        dispatch({ type: setGlobalParam, storage: 'return_toggle', response: !return_toggle })
    }

    render() {
        const {
            return_toggle
        } = this.props;
        return (
            <div className="accordion-toggle">
                <Icon material="insert_chart"></Icon>
                <Toggle name='return_toggle' checked={return_toggle} className="no-fastclick" onChange={(e) => this.handleToggle(e)} color="blue" />
                <Icon material="list"></Icon>
            </div>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ReturnAccordion);
