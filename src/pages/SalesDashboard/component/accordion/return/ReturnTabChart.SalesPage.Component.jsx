import React, { Component } from "react";
import { Bar } from 'react-chartjs-2';
import { connect } from "react-redux";
import { setGlobalParam } from "../../../../../global/global.Reducer";

class ReturnAccordion extends Component {
    componentDidUpdate(prevProps) {
        const {
            return_accordion_data,
        } = this.props;

        if (return_accordion_data != null && return_accordion_data != [] && return_accordion_data != prevProps.return_accordion_data) {
            this.calcChartData();
        }

    }
    calcChartData() {
        const { dispatch, date_type,
            return_target_obj, return_accordion_data
        } = this.props;
        let labels = [];
        let target_data = [];
        let amt_data = [];
        let data = {};
        let datasets = [];


        Object.keys(return_target_obj).map((key, index) => {
            target_data[index] = 0;
            if (typeof return_target_obj[key] == 'number') {
                let target_value = return_target_obj[key] - return_accordion_data[key];
                target_data[index] = target_value < 0 ? 0 : target_value.toFixed(2);
            }
        });

        Object.keys(return_accordion_data).map((key, index) => {
            amt_data[index] = 0;
            if (typeof return_accordion_data[key] == 'number') {
                labels[index] = key;
                amt_data[index] = return_accordion_data[key];
            }
        });


        datasets = [
            {
                label: 'Amount',
                backgroundColor: '#2196F3',
                data: amt_data,
                stack: 1
            },
            {
                label: 'Target',
                data: target_data,
                stack: 1
            },
        ];

        if (date_type == 'day' || date_type == 'month') {
            datasets = [
                {
                    label: 'Amount',
                    backgroundColor: '#2196F3',
                    data: amt_data,
                    stack: 1
                }
            ];
        }

        data = {
            labels: labels,
            datasets: datasets
        };

        dispatch({ type: setGlobalParam, storage: 'return_chart_data', response: data });
    }

    

    render() {
        const {
            sales_target_obj,
            return_chart_data
        } = this.props;
        return (
            <div className="card">
                <div className="card-content card-content-padding">
                    <Bar
                        data={return_chart_data}
                        width={100}
                        height={200}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                xAxes: [{ stacked: true }],
                                yAxes: [{ stacked: true }]
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: (t, d) => {
                                        if (t.datasetIndex == 0) {
                                            return t.yLabel
                                        }
                                        if (t.datasetIndex == 1) {
                                            return sales_target_obj[t.xLabel] ? sales_target_obj[t.xLabel] : ''
                                        }
                                    }
                                }
                            },
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ReturnAccordion);
