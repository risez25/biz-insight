import { Tab, Tabs } from "framework7-react";
import React, { Component } from "react";
import { connect } from "react-redux";
import ReturnToggle from "./ReturnToggle.SalesPage.Component";
import ReturnTabList from "./ReturnTabList.SalesPage.Component";
import ReturnTabChart from "./ReturnTabChart.SalesPage.Component";

class ReturnAccordion extends Component {
    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const {
            total_return_amt, return_toggle
        } = this.props;
        return (
            <>
                <a href="#" className="item-content item-link" style={{ fontSize: 12, paddingLeft: 0 }}><div className="item-inner" style={{ backgroundColor: '#E5E5E5' }}>
                    <div className="item-title" style={{ minHeight: 65 }}>
                        <p style={{ paddingTop: 6, fontSize: 16, paddingLeft: 16 }}>Return</p>
                    </div>
                    <div className="item-after">
                        {<p className="item-after-amt">{this.numberFormat(total_return_amt)}</p>}
                    </div>
                </div></a>
                <div className="accordion-item-content">
                    <div className="block">
                        <ReturnToggle />
                        <Tabs>
                            <Tab id="tab-1" className={`${return_toggle ? 'tab-active' : ''}`} >
                                <ReturnTabList />
                            </Tab>
                            <Tab id="tab-2" className={`${return_toggle ? '' : 'tab-active'}`}>
                                <ReturnTabChart />
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(ReturnAccordion);
