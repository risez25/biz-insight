import React, { Component } from "react";
import { Bar } from 'react-chartjs-2';
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class SalesTabChart extends Component {
    calcChartData() {
        const { dispatch, date_type,
            sales_target_obj, sales_accordion_data,
        } = this.props;
        let labels = [];
        let target_data = [];
        let amt_data = [];
        let sales_data = [];
        let data = {};
        let datasets = [];

        Object.keys(sales_target_obj).map((key, index) => {
            target_data[index] = 0;
            if (typeof sales_target_obj[key] == 'number') {
                let target_value = sales_target_obj[key] - (sales_accordion_data.inv[key] + sales_accordion_data.so[key]);
                target_data[index] = target_value < 0 ? 0 : target_value.toFixed(2);
            }
        });

        Object.keys(sales_accordion_data.inv).map((key, index) => {
            amt_data[index] = 0;
            if (typeof sales_accordion_data.inv[key] == 'number') {
                labels[index] = key;
                amt_data[index] = sales_accordion_data.inv[key];
            }
        });

        Object.keys(sales_accordion_data.so).map((key, index) => {
            sales_data[index] = 0;
            if (typeof sales_accordion_data.so[key] == 'number') {
                labels[index] = key;
                sales_data[index] = sales_accordion_data.so[key];
            }
        });


        datasets = [
            {
                label:  'Invoice',
                backgroundColor: '#2196F3',
                data: amt_data,
                stack: 1
            },
            {
                label: 'Target',
                // backgroundColor: '#90CAF9',
                data: target_data,
                stack: 1
            },
        ];

        if (date_type == 'day' || date_type == 'month') {
            datasets = [
                {
                    label: 'Invoice',
                    backgroundColor: '#2196F3',
                    data: amt_data,
                    stack: 1
                }
            ];
        }

        datasets.unshift({
            label: 'WIP S/O',
            backgroundColor: 'green',
            data: sales_data,
            stack: 1
        })
        data = {
            labels: labels,
            datasets: datasets
        };

        dispatch({ type: setGlobalParam, storage: 'sales_chart_data', response: data });
    }
    componentDidUpdate(prevProps) {
        const {
            sales_accordion_data,
        } = this.props;

        if (sales_accordion_data != null && sales_accordion_data != [] && sales_accordion_data != prevProps.sales_accordion_data) {
            this.calcChartData();
        }

    }

    render() {
        const {
            sales_chart_data,
        } = this.props;
        return (
            <div className="card">
                <div className="card-content card-content-padding">
                    <Bar
                        data={sales_chart_data}
                        width={100}
                        height={200}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                xAxes: [{ stacked: true }],
                                yAxes: [{ stacked: true }]
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                                callbacks: {
                                    label: (t, d) => {
                                        return `${d.datasets[t.datasetIndex].label} ${d.datasets[t.datasetIndex].data[t.index]}`
                                    }
                                }
                            },
                        }}
                    />
                </div>
            </div>

        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesTabChart);
