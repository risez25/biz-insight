import { Block, Col, Icon, Label, ListItem, Progressbar, Row, Tab, Tabs, Toggle } from "framework7-react";
import moment from 'moment';
import React, { Component } from "react";
import { Bar } from 'react-chartjs-2';
import { connect } from "react-redux";
import SalesToggle from "./SalesToggle.SalesPage.Component";
import SalesTabList from "./SalesTabList.SalesPage.Component";
import { setGlobalParam } from './../../../../../global/global.Reducer';
import SalesTabChart from "./SalesTabChart.SalesPage.Component";

class SalesAccordion extends Component {
    constructor(props) {
        super(props)
        this.sumTarget = this.sumTarget.bind(this);
    }

    sumTarget(object, storage_name) {
        const { dispatch, date_type, date_from } = this.props;
        let data = 0
        switch (date_type) {
            case 'year':
                Object.keys(object).map((key) => {
                    if (typeof object[key] == 'number') {
                        data = parseFloat(data) + parseFloat(object[key]);
                    }
                });
                break;
            case 'month':
                data = object[moment(date_from).format('MMM')];
                break;
            default:
                break;
        }
        data = data == NaN || data == undefined ? 0 : data;
        dispatch({ type: setGlobalParam, storage: storage_name, response: data })
    }

    componentDidUpdate(prevProps) {
        const {
            //sales param
            sales_accordion_data, sales_target_obj, total_sales_amt, total_sales_target, total_inv_amt,
            dispatch, date_type, show_loading, date
        } = this.props;

        // get total target
        if (
            sales_target_obj != null && sales_target_obj != {} && sales_target_obj != prevProps.sales_target_obj
            || date_type != prevProps.date_type
            || date != prevProps.date
        ) {
            this.sumTarget(sales_target_obj, 'total_sales_target');
        }

        //get total percentage
        if (total_sales_target != 0 || total_sales_target != prevProps.total_sales_target) {
            dispatch({ type: setGlobalParam, storage: 'total_sales_percentage', response: parseFloat((((parseFloat(total_sales_amt) + parseFloat(total_inv_amt)) / parseFloat(total_sales_target)) * 100).toFixed(2)) })
        }
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const {
            total_sales_percentage, total_sales_amt, total_inv_amt, total_sales_target,
            date_type, sales_toggle,
            is_sales_accordion_open
        } = this.props;
        const total_sales_inv_amt = parseFloat(total_inv_amt) + parseFloat(total_sales_amt);
        return (
            <>
                <a href="#" className="item-content item-link" style={{ fontSize: 12, paddingLeft: 0 }}><div className="item-inner" style={{ backgroundColor: '#E5E5E5' }}>
                    <div className="item-title" style={{ minHeight: 65 }}>
                        <Row>
                            <Col width={100}>
                                {
                                    date_type != 'day' ?
                                        <div>
                                            <p className="item-title-month">Sales</p>
                                            <div style={{ marginTop: '-35px', paddingLeft: 16 }}>
                                                <p style={{ marginBottom: '-7px', textAlign: 'right', top: 7, fontSize: 12 }}>{"Target: " + this.numberFormat(total_sales_target)}</p>
                                                <Progressbar color="blue" style={{ top: 10 }} progress={parseFloat(total_sales_percentage) > 100 ? 100 : parseFloat(total_sales_percentage)}></Progressbar>
                                                <p style={{ marginBottom: '-7px', textAlign: 'left', top: 15, fontSize: 12 }}>{this.numberFormat(total_sales_inv_amt) + " (" + this.numberFormat(total_sales_amt) + " + " + this.numberFormat(total_inv_amt) + ")"}</p>
                                            </div>
                                        </div>
                                        :
                                        <p className="item-title-day">Sales</p>
                                }
                            </Col>
                        </Row>
                        {/* <div className="item-footer" >{parseFloat(accordion_data[key])}</div> */}
                    </div>
                    <div className="item-after">
                        {date_type != 'day' ?
                            <Label>{parseFloat(total_sales_percentage) > 100 ? '100%' : parseFloat(total_sales_percentage) + '%'}</Label> :
                            <p className="item-after-amt">{this.numberFormat((parseFloat(total_sales_amt) + parseFloat(total_inv_amt)))}</p>
                        }
                    </div>

                </div></a>
                <div className="accordion-item-content">
                    <div className="block">
                        <SalesToggle />
                        <Tabs style={{ paddingTop: 0 }}>
                            <Tab id="tab-1" className={`${sales_toggle ? 'tab-active' : ''}`} >
                                <SalesTabList />
                            </Tab>
                            <Tab id="tab-2" className={`${sales_toggle ? '' : 'tab-active'}`}>
                                <SalesTabChart />
                            </Tab>
                        </Tabs>
                    </div>
                </div>
            </>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesAccordion);
