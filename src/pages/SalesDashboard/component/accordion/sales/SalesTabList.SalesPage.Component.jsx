import { Block, Col, Icon, Label, ListItem, Progressbar, Row, Tab, Tabs, Toggle } from "framework7-react";
import moment from 'moment';
import React, { Component } from "react";
import { connect } from "react-redux";

class SalesTabList extends Component {
    dateDayList() {
        const { date_from } = this.props;

        let date = moment(date_from);
        let date_start = moment(date.startOf('isoWeek'));
        let date_end = moment(date.endOf('isoWeek'));

        let date_list = [];
        while (date_start < date_end) {
            let date_array = { from: '', to: '' };
            date_array.from = moment(date_start).format('DD/MM');
            date_array.to = moment(date_start).format('DD/MM');
            date_start = moment(date_start).add(1, 'day');
            date_list.push(date_array);
        }

        return date_list

    }

    dateMonthList() {
        const { date_from, date_to } = this.props;
        let from = moment(date_from);
        let to = moment(date_to);
        let date_index = from;
        let date_list = [];
        while (date_index < to) {
            let date_array = { from: '', to: '' };
            date_array.from = moment(date_index).format('DD/MM');
            date_index = moment(date_index).add(1, 'week').startOf('isoWeek');
            date_array.to = moment(date_index).subtract(1, 'day').format('DD/MM');
            date_list.push(date_array);
        }
        date_list[date_list.length - 1].to = moment(date_to).format('DD/MM');
        return date_list;
    }

    renderList() {
        const {
            date_type,
            sales_accordion_data, sales_target_obj
        } = this.props;
        let { date_footer, date_list, list, accordion_data, sales_data, target_obj } = ''

        accordion_data = sales_accordion_data.inv;
        sales_data = sales_accordion_data.so;
        target_obj = sales_target_obj;

        switch (date_type) {
            case 'day':
                date_list = this.dateDayList();
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    date_footer = date_list[index] ? date_list[index]['from'] : '';

                    let total_so_inv = parseFloat(sales_data[key]) + parseFloat(accordion_data[key]);
                    return (
                        <ListItem key={index} title={key} footer={date_footer}>
                            <div slot='after'>
                                <Row>
                                    <Label style={{ marginLeft: '43%' }} >{total_so_inv}</Label>
                                </Row>
                                <Row>
                                    <Col width="unset"><Label>(</Label></Col>
                                    <Col width="unset"><Label style={{ color: 'green' }}>{`${this.numberFormat(sales_data[key])}`}</Label></Col>
                                    <Col width="unset"><Label>+</Label></Col>
                                    <Col width="unset"><Label style={{ color: 'blue' }}>{` ${this.numberFormat(accordion_data[key])}`}</Label></Col>
                                    <Col width="unset"><Label>)</Label></Col>
                                </Row>

                            </div>
                        </ListItem>
                    );
                }) : "don't have data";
                break;
            case 'month':
                date_list = this.dateMonthList();
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    date_footer = date_list[index] ? date_list[index]['from'] + ' - ' + date_list[index]['to'] : '';

                    let total_so_inv = parseFloat(sales_data[key]) + parseFloat(accordion_data[key]);
                    return (
                        <ListItem key={index} title={key} footer={date_footer}>
                            <div slot='after'>
                                <Row>
                                    <Label style={{ marginLeft: '43%' }} >{total_so_inv}</Label>
                                </Row>
                                <Row>
                                    <Col width="unset"><Label>(</Label></Col>
                                    <Col width="unset"><Label style={{ color: 'green' }}>{`${this.numberFormat(sales_data[key])}`}</Label></Col>
                                    <Col width="unset"><Label>+</Label></Col>
                                    <Col width="unset"><Label style={{ color: 'blue' }}>{` ${this.numberFormat(accordion_data[key])}`}</Label></Col>
                                    <Col width="unset"><Label>)</Label></Col>
                                </Row>
                            </div>
                        </ListItem>
                    );
                }) : "don't have data";
                break;
            case 'year':
                list = accordion_data != null && accordion_data != [] ? Object.keys(accordion_data).map((key, index) => {
                    let percent = ((parseFloat(accordion_data[key]) / parseFloat(target_obj[key])) * 100).toFixed(2);
                    let percentages = percent == "NaN" ? 0 : parseFloat(percent);

                    return (
                        <li key={index} >
                            <div className="item-content" style={{ fontSize: 12, paddingLeft: 0 }}>
                                <div className="item-inner" style={{ paddingTop: 0, paddingBottom: 0, marginBottom: '-10px' }}>
                                    <div className="item-title" style={{ minHeight: 65 }}>
                                        <Row>
                                            <Col width={15}>
                                                <p style={{ paddingTop: 13, fontSize: 14 }}>{key}</p>
                                            </Col>
                                            <Col width={85}>
                                                <p style={{ marginBottom: '-7px', textAlign: 'right', top: 7 }}>{this.numberFormat(target_obj[key])}</p>
                                                <Progressbar color={
                                                    percentages > 99 ? 'green' :
                                                        percentages > 50 ? 'orange' :
                                                            'red'
                                                } style={{ top: 10 }} progress={percentages > 100 ? 100 : percentages}></Progressbar>
                                                <div style={{ marginBottom: '-7px', textAlign: 'left', top: 15, fontSize: 11 }}>{
                                                    <div>
                                                        <Row>
                                                            <Label style={{ marginLeft: '65%' }} >{this.numberFormat((parseFloat(sales_data[key]) + parseFloat(accordion_data[key])))}</Label>
                                                        </Row>
                                                        <Row style={{ justifyContent: 'flex-end' }}>
                                                            <Col width="unset"><Label>(</Label></Col>
                                                            <Col width="unset"><Label style={{ color: 'green' }}>{`${this.numberFormat(sales_data[key])}`}</Label></Col>
                                                            <Col width="unset"><Label>+</Label></Col>
                                                            <Col width="unset"><Label style={{ color: 'blue' }}>{` ${this.numberFormat(accordion_data[key])}`}</Label></Col>
                                                            <Col width="unset"><Label>)</Label></Col>
                                                        </Row>
                                                    </div>
                                                }</div>
                                            </Col>
                                        </Row>
                                    </div>
                                    <div className="item-after">
                                        <Label>{parseFloat(percentages) > 100 ? '100%' : parseFloat(percentages) + '%'}</Label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    )
                }) : "don't have data";
                break;
            default:
                break;
        }
        return list;
    }

    numberFormat(number) {
        return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    render() {
        const sales_list = this.renderList();
        return (
            <Block>
                <ul>
                    {sales_list}
                </ul>
            </Block>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesTabList);
