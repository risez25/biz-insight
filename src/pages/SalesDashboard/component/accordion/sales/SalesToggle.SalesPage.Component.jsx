import { Icon, Toggle } from "framework7-react";
import React, { Component } from "react";
import { connect } from "react-redux";
import { setGlobalParam } from './../../../../../global/global.Reducer';

class SalesToggle extends Component {
    handleToggle() {
        const { dispatch, sales_toggle } = this.props;
        dispatch({ type: setGlobalParam, storage: 'sales_toggle', response: !sales_toggle })
    }

    render() {
        const {
            sales_toggle 
        } = this.props;
        return (
            <div className="accordion-toggle">
                <Icon material="insert_chart"></Icon>
                <Toggle name='sales_toggle' checked={sales_toggle} className="no-fastclick" onChange={(e)=>this.handleToggle(e)} color="blue" />
                <Icon material="list"></Icon>
            </div>
        );
    }
}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(SalesToggle);
