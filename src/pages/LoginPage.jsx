import CryptoJS from "crypto-js";
import { Button, Input, Label, List, ListItem, LoginScreen, LoginScreenTitle, Page, View, Card, Block } from 'framework7-react';
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import { renderField, required } from "../components/input";
import { getEvent, postEvent } from '../global/global.Saga';
import { setGlobalParam } from "../global/global.Reducer";
import mainIcon from "../image/Bizinsight.jpeg";
class LoginPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            postApi: 'bizInsight/login',
            remember_me: localStorage.getItem('remember_me') ? JSON.parse(localStorage.getItem('remember_me')) : false,
            username: localStorage.getItem('username') ? localStorage.getItem('username') : false,
            password: localStorage.getItem('password') ? localStorage.getItem('password') : false,
        }
        const { postApi } = this.state;
        const { dispatch } = props;

        this.showLoading = () => dispatch({ type: setGlobalParam, storage: 'show_loading', response: true });

        this.postEvent = () => dispatch(postEvent({ api: postApi }));
    }

    componentWillMount() {
        const { username, password, remember_me } = this.state;
        if (username && password && remember_me) {
            this.props.change('username', username);
            let bytes = CryptoJS.AES.decrypt(password, 'bizinsight');
            let plaintext = bytes.toString(CryptoJS.enc.Utf8);
            this.props.change('password', plaintext);
            this.postEvent();

            this.showLoading();
        }

    }
    setRememberMe() {
        const { remember_me } = this.state;
        this.setState({ remember_me: !remember_me });
        localStorage.setItem('remember_me', !remember_me);
    }

    onSubmit() {
        this.postEvent();
    }
    render() {
        const { handleSubmit, isAuthenticated } = this.props;
        const { remember_me } = this.state;

        return (
            <LoginScreen id="login-screen">
                <View>
                    <Page>
                        <LoginScreen opened={!isAuthenticated}>
                            <Card style={{height:'max-content'}} >
                                <Block style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }} >
                                    <img src={mainIcon} style={{ height: '30%', width: '30%', maxHeight:'fit-content', maxWidth:'fit-content' }} alt="not found" />
                                </Block>
                                <LoginScreenTitle>Biz-Insight</LoginScreenTitle>
                                <form onSubmit={handleSubmit} >
                                    <Card>
                                        <List >
                                            <ListItem>
                                                <Field
                                                    name="username"
                                                    type="text"
                                                    component={renderField}
                                                    label="Username"
                                                    validate={required}
                                                    placeholder="Username"
                                                />
                                            </ListItem>
                                            <ListItem>
                                                <Field
                                                    name="password"
                                                    type="password"
                                                    component={renderField}
                                                    label="Password"
                                                    validate={required}
                                                    placeholder="Password"
                                                />
                                            </ListItem>
                                            <ListItem>
                                                <div>
                                                    <Label>Remember me next</Label>
                                                    <Input type="checkbox" checked={remember_me} onChange={(e) => this.setRememberMe(e)} />
                                                </div>
                                            </ListItem>

                                            <ListItem>
                                                <Button type="submit" onClick={(e) => this.onSubmit(e)} >Sign In</Button>
                                            </ListItem>

                                        </List>
                                    </Card>
                                </form>
                            </Card>
                        </LoginScreen>

                    </Page>
                </View>
            </LoginScreen>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    dispatch
});

const mapStateToProps = state => {
    return {
        isAuthenticated: state.global.isAuthenticated,
    };
};

const LoginForm = reduxForm({
    form: 'form',
    enableReinitialize: true
})(LoginPage);

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
