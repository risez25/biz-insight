import { Link, List, Navbar, NavLeft, Button, AccordionContent, ListItem, Page, Block } from 'framework7-react';
import React, { Component } from 'react';
import { connect } from "react-redux";
import Dom7 from 'dom7';
import { setGlobalParam, logout } from '../global/global.Reducer';
import mainIcon from "../image/Bizinsight.jpeg";
class LoginPage extends Component {

  logOut() {
    const { dispatch } = this.props;

    localStorage.removeItem('remember_me');
    dispatch({ type: setGlobalParam, storage: 'isAuthenticated', response: false })
    dispatch({ type: setGlobalParam, storage: 'remember_me', response: false });
    dispatch({ type: logout });
    var $$ = Dom7;
    $$('#salesteam .item-after').html('');
    $$('#salesman .item-after').html('All');
  }

  render() {
    const { role, username } = this.props;
    return (
      <Page className='panel-left' style={{ background: 'white', color: 'black' }}>
        <Navbar title="Biz-Insight">
          <NavLeft slot='backLink'>
            <img src={mainIcon} alt="Image not found" />
          </NavLeft>
        </Navbar>
        <List style={{margin:'unset'}}>
          <ListItem style={{backgroundColor:'lightgray'}}>
            <Block style={{margin:5}}>{username}</Block>
          </ListItem>

          <ListItem link="/" title="Sales Dashboard" panelClose></ListItem>

          {role != 'salesman' ?
            <ListItem accordionItem accordionItemOpened title="Comparison" style={{zIndex:1}} >
              <AccordionContent>
                <List>
                  <ListItem link="/comparison/Salesman(Sales)" title="Salesman (Sales)" panelClose></ListItem>
                  <ListItem link="/comparison/Salesman(Collection)" title="Salesman (Collection)" panelClose></ListItem>
                  <ListItem link="/comparison/Salesteam" title="SalesTeam" panelClose></ListItem>
                  <ListItem link="/comparison/Product" title="Product" panelClose></ListItem>
                </List>
              </AccordionContent>
            </ListItem>
            : ''}
          <ListItem panelClose>
            <Button slot='title' panelClose onClick={(e) => this.logOut(e)} >Log Out</Button>
          </ListItem>
          <ListItem style={{ bottom: 0, position: 'fixed', fontSize: '12px' }}>
            <p>EFICHAIN SOLUTION SDN BHD</p>
          </ListItem>
        </List>
      </Page>
    )
  }

}

export default connect(state => ({ ...state.global }), dispatch => ({ dispatch }))(LoginPage);