import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from 'redux-form'
import { routerReducer } from "react-router-redux";

import global from "../global/global.Reducer";

export default combineReducers({
    global,
    form: reduxFormReducer,
    router: routerReducer,
});

