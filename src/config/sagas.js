import "regenerator-runtime/runtime";
import { all } from "redux-saga/effects";

import { global } from "../global/global.Saga";
export default function* rootSaga() {
    yield all([
        ...global,
    ]);
}
