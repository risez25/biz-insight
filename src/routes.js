import SalesDashboardPage from './pages/SalesDashboard/SalesDashboardPage';
import PanelLeftPage from './pages/PanelLeftPage';
import ComparisonPage from './pages/Comparison/ComparisonPage';
import LoginPage from './pages/LoginPage';

export default [
  {
    path: '/',
    component: SalesDashboardPage,
  },
  {
    path: '/login/',
    component: LoginPage,
  },
  {
    path: '/comparison/:child_route',
    component: ComparisonPage,
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage,
  },
];
