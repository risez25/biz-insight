import React, { Component } from 'react';
import {
  App,
  Panel,
  View,
  Statusbar,
  Popup,
  Page,
  Navbar,
  NavRight,
  Link,
  Block,
} from 'framework7-react';
import { ToastContainer, Slide } from 'react-toastify';
import routes from './routes';
import LoginPage from './pages/LoginPage';
import { connect } from "react-redux";
const f7params = {
  id: 'io.framework7.testapp', // App bundle ID
  name: 'Framework7', // App name
  theme: 'auto', // Automatic theme detection
  routes,
  panel: {
    swipe: 'left',
    leftBreakpoint: 1002,
  },
};

class AppContainer extends Component {
  componentDidUpdate(prevProps) {
    const { show_loading } = this.props;

    if (show_loading != prevProps.show_loading) {
      if (show_loading) {
        this.$f7.preloader.show();
      } else {
        this.$f7.preloader.hide();
      }
    }
  }
  render() {
    return (
      <App params={f7params}>
        {/* Statusbar */}
        <Statusbar />
        <ToastContainer
          position="top-right"
          transition={Slide}
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnVisibilityChange
          draggable
          pauseOnHover
          style={{ zIndex: 22000 }}
        />

        {/* Left Panel */}
        <Panel left reveal>
          <View url="/panel-left/" linksView=".view-main" />
        </Panel>

        {/* Main View */}
        <View id="main-view" url="/" main className="ios-edges" />

        {/* Login Screen */}
        <LoginPage />
      </App>
    );
  }
}
const mapDispatchToProps = dispatch => ({
  dispatch
});

const mapStateToProps = state => {
  return {
    ...state.global
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
